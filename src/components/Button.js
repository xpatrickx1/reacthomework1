import React from 'react'

class Button extends React.Component {
    constructor (props) {
        super(props);
        this.props = {backgroundColor: '', text: '', className: ''};
    }

    render () {
        return (
            <button className={this.props.className}
                    style={this.props.backgroundColor}
            >
                    {this.props.text}
            </button>
        )
    }
}

export default Button
