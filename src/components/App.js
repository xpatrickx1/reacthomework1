/** Core */
import React from 'react';

/** Styles*/
import '../App.css';
import '../Modal.css';
import './Button.css';

/** Components */
import Modal from './Modal';
import Button from "./Button";


class App extends React.Component {
    state = {
        showModal: false,
        actions: [<Button className='trigger-btn'
                          backgroundColor={{background: '#004256'}}
                          text='Ok'/>,
                  <Button className='trigger-btn'
                          backgroundColor={{background: '#004256'}}
                          text='Cancel'/>, ]
    };


    toggleModal = (attr) => {
        this.setState({
            showModal: !this.state.showModal,
            attr: attr,
        });
    };

    render() {

        return (
            <div className="App">
                <h1>Hello React</h1>
                <span
                    onClick={() => this.toggleModal("first")}
                    className="trigger-btn"
                >
					Open First Modal
                </span>

                <span
                    onClick={() => this.toggleModal("second")}
                    className="trigger-btn"
                >
					Open Second Modal
                </span>

                { this.state.showModal &&
                this.state.attr==='first' && (
                <Modal attr="first"
                       closeButton={true}
                       header="First modal"
                       text="My first React Modal"
                       actions={this.state.actions}
                       toggleModal={this.toggleModal}>
                    Message in Modal
                </Modal>
                )}

                { this.state.showModal &&
                this.state.attr==='second' && (
                    <Modal attr="second"
                           closeButton={this.state.closeButton}
                           header="Second modal"
                           text="My second React Modal"
                           actions={this.state.actions}
                           toggleModal={this.toggleModal}>
                        Message in Modal
                    </Modal>
                )}
             </div>
        );
    };

};


export default App;
