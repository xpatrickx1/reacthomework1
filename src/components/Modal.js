import React from 'react'
import PropTypes from 'prop-types';


class Modal extends React.Component {
    constructor (props) {
        super(props);
        this.props = {header: '', closeButton: true, text: '', actions: [], attr: ''};
    }
    state = {
        showModal: false,
    };

    render () {
        const isCloseBtn = this.props.closeButton;
        const actions = this.props.actions.map(((item, index) => {
            return <div key={index}>{item}</div>
        }));
        let button = null;
        if(isCloseBtn){
            button = <span className="close" onClick={() => { this.props.toggleModal(this.state.showModal)}}>&times;</span>
        } else {
            button = null
        }

        return (
            <div  className="modal" attr={this.props.attr} id='modal' onClick={
                (event) => {
                    if (event.target === document.getElementById('modal')) {
                        this.props.toggleModal(this.state.showModal);
                    }
                }
            }>
                <div className="modal-content" >
                    <div className="modal-header">
                        <h1 className="modal-title">{this.props.header}</h1>
                        {button}
                    </div>
                    <div className="modal-body">
                        <p className="modal-text">{this.props.text}</p>
                    </div>

                    <div className="modal-footer">
                        {actions}
                    </div>
                </div>
            </div>
        );
    };
}

Modal.propTypes= {
    toggleModal: PropTypes.func,
    showModal: PropTypes.bool
};



export default Modal